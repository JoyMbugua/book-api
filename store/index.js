import { v4 as uuidv4 } from 'uuid'

export const state = () => ({

  booksData: [],
  cart: []
})

export const getters = {
  totalItems: state => {
    return state.cart.length
  },
  totalCost: state => {
    return state.cart.reduce((acc, item) => acc + +item.price, 0)
  }
}

export const mutations = {
  updateBooks: (state, payload) => {
    state.booksData = payload
  },
  addToCart(state, formOutput){
    formOutput.id = uuidv4()
    state.cart.push(formOutput)
  },
  removeItem(state, item){
    let id = item.id
    for(let i = 0; i < state.cart.length; i++){
      if(state.cart[i].id === id){
        delete state.cart[i]
        break
      }
    }
  }
}

export const actions = {
  async getBooks({ commit, state }) {
    try{
      
      await fetch('bookupapi.herokuapp.com/api/?format=json').then(response => response.json()).then(payload =>{
        console.log("data",payload)
        commit('updateBooks', payload)
      })
    }catch(error){
      console.log(error)
    }
  }
}